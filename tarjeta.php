<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>
    <script type="text/javascript">
        Conekta.setPublicKey('llave-publica');

        var conektaSuccessResponseHandler = function (token) {
            var $form = $("#card-form");
            //Inserta el token_id en la forma para que se envíe al servidor
            $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
            $form.get(0).submit(); //Hace submit
        };
        var conektaErrorResponseHandler = function (response) {
            var $form = $("#card-form");
            $form.find(".card-errors").text(response.message_to_purchaser);
            $form.find("button").prop("disabled", false);
        };

        //jQuery para que genere el token después de dar click en submit
        $(function () {
            $("#card-form").submit(function (event) {
                var $form = $(this);
                // Previene hacer submit más de una vez
                $form.find("button").prop("disabled", true);
                Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
                return false;
            });
        });
    </script>
    <title>Document</title>
</head>
<body>
<?php

$alfabeto = array("a", "b", "c", "d", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "x", "z");
$alfabeto_random = null;
for($i = 0; $i < count($alfabeto); $i++) {
    $random_number=rand(0, (count($alfabeto) - 1));
    $alfabeto_random.= $alfabeto[$random_number];
    if($i == 5)
        break;

}
?>
<form action="process-order.php" method="POST" id="card-form">
    <span class="card-errors"></span>
    <div>
        <label>
            <span>Nombre del tarjetahabiente</span>

            <input type="text" size="20" name="name" data-conekta="card[name]" value="Develuxmx <?=$alfabeto_random?>">
        </label>
    </div>

    <div>
        <label>
            <span>Correo electrónico</span>
            <input type="text" name="email" value="<?=$alfabeto_random?>@tucorreo.com">
        </label>
    </div>

    <div>
        <label>
            <span>Número de tarjeta de crédito</span>
            <input type="text" size="20" name="number" data-conekta="card[number]" value="4242424242424242">
        </label>
    </div>
    <div>
        <label>
            <span>CVC</span>
            <input type="text" size="4" data-conekta="card[cvc]" value="123">
        </label>
    </div>
    <div>
        <label>
            <span>Fecha de expiración (MM/AAAA)</span>
            <input type="text" size="2" data-conekta="card[exp_month]" value="<?=date('m')?>">
        </label>
        <span>/</span>
        <input type="text" size="4" data-conekta="card[exp_year]" value="<?=date('Y', strtotime('+ 2 years'))?>">
    </div>
    <button type="submit">Pagar</button>
</form>
</body>
</html>