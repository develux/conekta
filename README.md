# REQUERIMIENTOS / REQUIREMENTS
PHP 7.*

# INSTRUCCIONES / INSTRUCTIONS

1.- Descargar repositorio de conekta-php de la sig. URL / Download conekta-php repository from next url.
https://github.com/conekta/conekta-php

2.- Reemplazar el texto llave-publica por tu api-key público de conekta en el archivo tarjeta.php / Replace the llave-publica text with your public api-key of conekta in the file tarjeta.php.

3.- Reemplazar el texto llave-privada por tu api-key privado de conekta en los archivos process-order-oxxo.php y process-order.php / Replace the llave-privada text with your conekta private api-key in the process-order-oxxo.php and process-order.php files.

4.- Navegar en la raíz del proyecto desde el navegador y seleccionar tu método de pago / Browse the root of the project from the browser and select your payment method.
