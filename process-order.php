<?php
session_start();
$carrito = $_SESSION["cart"];
require_once("conekta-php/lib/Conekta.php");
\Conekta\Conekta::setApiKey("llave-privada");
\Conekta\Conekta::setApiVersion("2.0.0");

try {
    $items = [];
    $i = 0;
    foreach ($carrito["items"] as $cart) {
        $items[$i]["name"] = $cart["nombre"];
        $items[$i]["unit_price"] = $cart["precio"];
        $items[$i]["quantity"] = $cart["qty"];
        $i++;
    }

    // Creamos un objeto order y enviamos la información del pedido.
    $order = \Conekta\Order::create(
        array(
            "line_items" => $items,
            "currency" => "MXN",
            "customer_info" => array(
                "name" => $_POST["name"],
                "email" => $_POST["email"],
                "phone" => "6123433333"
            ),
            "charges" => array(
                array(
                    "payment_method" => array(
                        "type" => "card",
                        "token_id" => $_REQUEST["conektaTokenId"]
                    )
                )
            )
        )
    );


    // Almacenamos el estatus del cargo en una variable.
    $status_payment = $order->payment_status;

    // Cachamos el estado del cargo y tomamos decisiones
    switch ($status_payment) {
        case "paid" :
            echo "El cargó se aplicó en la orden: " . $order->id . "<br />"; // El reference nos puede servir para insertar la información de la orden en base de datos y ligarla.
            echo "<button onclick='location.href=\"index.php\"'>Volver</button>";
            break;

        case "declined" :
            echo "El cargo fue declinado";
            break;

        case "expired" :
            echo "Cargo expirado";
            break;
    }

    // Cachamos los errores que provienen directamente del sistema.
} catch (\Conekta\ProcessingError $error) {
    echo $error->getMessage();
} catch (\Conekta\ParameterValidationError $error) {
    echo $error->getMessage();
} catch (\Conekta\Handler $error) {
    echo $error->getMessage();
}
?>