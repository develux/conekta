<?php
session_start();
$carrito = $_SESSION["cart"];
require_once("conekta-php/lib/Conekta.php");
\Conekta\Conekta::setApiKey("llave-privada");
\Conekta\Conekta::setApiVersion("2.0.0");

try {
    $items = [];
    $i = 0;

    // Armamos el arreglo con los productos
    foreach ($carrito["items"] as $cart) {
        $items[$i]["name"] = $cart["nombre"];
        $items[$i]["unit_price"] = ($cart["precio"] * 100); // Se tiene que agregar 2 "decimales" para enviar los montos, ejemplo 100 pesos = 10000, 1 peso = 100
        $items[$i]["quantity"] = $cart["qty"];
        $i++;
    }

    //Este código es solo para crear un nombre y correo random, puedes eliminar esta parte del código y sustituir las líneas con la información que tu traes de tu cliente.
    $alfabeto = array("a", "b", "c", "d", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "x", "z");
    $alfabeto_random = null;
    for($i = 0; $i < count($alfabeto); $i++) {
        $random_number=rand(0, (count($alfabeto) - 1));
        $alfabeto_random.= $alfabeto[$random_number];
        if($i == 5)
            break;

    }
    // TERMINA CÓDIGO GENERADO PARA CORREO Y NOMBRE

    // Creamos un objeto order y enviamos la información del pedido.
    $order = \Conekta\Order::create(
        array(
            "line_items" => $items,
            "currency" => "MXN",
            "customer_info" => array(
                "name" => "Develuxmx $alfabeto_random", // Sustituir por el nombre de tu cliente
                "email" => "$alfabeto_random@tucorreo.com", // Sustituir por el correo de tu cliente
                "phone" => "6123433333"
            ),
            "charges" => array(
                array(
                    "payment_method" => array(
                        "type" => "oxxo_cash",
                        "token_id" => $_REQUEST["conektaTokenId"]
                    )
                )
            )
        )
    );


    // Mostramos todos los datos del pago (Si manejas base de datos, puedes guardarlo con su estatus de pago pendiente y luego cambiarlo en el webhook buscando por el id de orden para ponerlo a estatus pagdao).
    echo "El cargó se aplicó en la orden: " .  $order->id . "<br />";
    echo "Método de pago:". $order->charges[0]->payment_method->service_name. "<br />";
    echo "Código de barras: " . "<img src='" . $order->charges[0]->payment_method->barcode_url ."' />" . "<br />";
    echo "Referencia original: " . $order->charges[0]->payment_method->reference . "<br />";
    echo "Referencia formateada: ". substr($order->charges[0]->payment_method->reference, 0,4). "-" . substr($order->charges[0]->payment_method->reference,4,4) . "-" . substr($order->charges[0]->payment_method->reference,8,4) . "-" . substr($order->charges[0]->payment_method->reference,12,2) . "<br />";
    echo "Total: $". $order->amount/100 . $order->currency. "<br />";
    echo "<button onclick='location.href=\"index.php\"'>Volver</button>";

    // Cachamos los errores que provienen directamente del sistema.
} catch (\Conekta\ProcessingError $error) {
    echo $error->getMessage();
} catch (\Conekta\ParameterValidationError $error) {
    echo $error->getMessage();
} catch (\Conekta\Handler $error) {
    echo $error->getMessage();
}
?>