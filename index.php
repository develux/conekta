<?php

session_start();


//Inicializaciñon del carrito

// Producto 1
$items = [];
$items[0]["nombre"] = "Producto1";
$items[0]["precio"] = 100;
$items[0]["qty"] = 2;
$items[0]["total"] = 200;
// Producto 2
$items[1]["nombre"] = "Producto2";
$items[1]["precio"] = 50;
$items[1]["qty"] = 3;
$items[1]["total"] = 150;
// Información del carrito
$info = [];
$info["subtotal"] = 350;
$info["descuento"] = 100;
$info["total"] = 250;
$info["order"] = rand(1111, 9999);

// Armado
$cart["items"] = $items;
$cart["info"] = $info;
$_SESSION["cart"] = $cart;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
</pre>

<h1>Información de tu carrito</h1>



<?php
$carrito = $_SESSION["cart"];
?>

<h2>Productos</h2>
<table>
    <tr>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Total</th>
    </tr>

<?php
foreach($carrito["items"] as $item) {
?>
    <tr>
        <th><?=$item["nombre"]?></th>
        <th><?=$item["precio"]?></th>
        <th><?=$item["qty"]?></th>
        <th><?=$item["total"]?></th>
    </tr>
<?php
}
?>
</table>

<h2>Información de pago</h2>

<table>
    <tr>
        <th>Subtotal</th>
        <th>Descuento</th>
        <th>Total</th>
    </tr>
    <tr>
        <th><?=$carrito["info"]["subtotal"]?></th>
        <th><?=$carrito["info"]["descuento"]?></th>
        <th><?=$carrito["info"]["total"]?></th>
    </tr>
</table>


<button onclick="location.href='tarjeta.php'">Pagar con tarjeta</button>
<button onclick="location.href='process-order-oxxo.php'">Pagar en oxxo</button>
</body>
</html>