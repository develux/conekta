<?php

$body = @file_get_contents('php://input');
$data = json_decode($body);
http_response_code(200); // Return 200 OK

if ($data->type == 'charge.paid') {
    // La siguiente información guarda el id de la orden en un archivo llamado data.txt, es como simulación de que el cargo fue pagado y así puedes actualizarlo en tu bd e incluso enviarle un correo electrónico avisándole que su pago fue recibido y se hará el proceso correspondiente.
    $file = fopen("data.txt", "a");
    $date = date("d-m-Y H:m:i");
    fwrite($file, "" . PHP_EOL);
    fwrite($file, "[WEBHOOK] $date" . PHP_EOL);
    fwrite($file, "Tu id de órden es: " . $data->data->object->order_id); // Con este orden id, podemos cambiar el estatus del pedido a pagado en base de datos Ejemplo: update ordenes set status='pagado' WHERE id_orden = '$data->data->object->order_id';
    fwrite($file, " <!-- TERMINA --> " . PHP_EOL);
    fclose($file);
}

?>